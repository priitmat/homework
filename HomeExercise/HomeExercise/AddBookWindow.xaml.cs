﻿using HomeExercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HomeExercise
{
    /// <summary>
    /// Interaction logic for AddBookWindow.xaml
    /// </summary>
    public partial class AddBookWindow : Window
    {
        DatabaseService _databaseService;
        List<Shelf> _availableShelves;

        public AddBookWindow()
        {
            InitializeComponent();
            _databaseService = DatabaseService.Instance;            
            _availableShelves = _databaseService.RetrieveAvailableShelves();
            comboBoxShelves.ItemsSource = _availableShelves;
            comboBoxShelves.DisplayMemberPath = "Number";
            comboBoxShelves.SelectionChanged += ComboBoxShelves_SelectionChanged;
            comboBoxNumbersInShelf.SelectionChanged += ComboBoxNumbersInShelf_SelectionChanged;
            
        }

        private void ComboBoxNumbersInShelf_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            textBoxIdentifierCode.Text = ((Shelf)comboBoxShelves.SelectedItem).Number + " - " + comboBoxNumbersInShelf.SelectedValue;
        }

        private void ComboBoxShelves_SelectionChanged(object sender, SelectionChangedEventArgs e)
        { 
            comboBoxNumbersInShelf.ItemsSource = _databaseService.RetrieveAvailableShelfBookNumbers(((Shelf)comboBoxShelves.SelectedValue).Id, 0);
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            var newBook = new Book();
            newBook.Title = textBoxTitle.Text;
            newBook.Name = textBoxAuthor.Text;
            newBook.Age = Int32.Parse(textBoxAge.Text);
            newBook.DaysForLoan = Int32.Parse(textBoxDaysForLoan.Text);
            var selectedShelf = (Shelf)comboBoxShelves.SelectedItem;
            newBook.ShelfId = selectedShelf.Id;
            newBook.NumberInShelf = (int)comboBoxNumbersInShelf.SelectedValue;
            _databaseService.insertData<Book>(newBook);
            BookWindow bookWindow = new BookWindow();
            bookWindow.Show();
            this.Close();
        }

       
    }
}
