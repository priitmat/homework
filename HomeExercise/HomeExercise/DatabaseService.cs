﻿using HomeExercise.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeExercise
{
    public class DatabaseService
    {        
        private static DatabaseService _instance;
        public static DatabaseService Instance
        {
            get
            {
                return _instance ?? (_instance = new DatabaseService());
            }
        }

        string _databasePath;

        public DatabaseService()
        {           
            _databasePath ="db_sqlite.db";
            var result = createDatabase(_databasePath);
        }

        private string createDatabase(string path)
        {
            try
            {
                var connection = new SQLiteConnection(path);
                connection.CreateTable<Client>();
                connection.CreateTable<Book>();
                connection.CreateTable<ClientBook>();
                connection.CreateTable<Shelf>();
                return "Database Created";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        public void insertData<T>(T data)
        {
            try
            {
                var db = new SQLiteConnection(_databasePath);
                db.Insert(data);                
                
            }
            catch (SQLiteException ex)
            {
                
            }
        }
        public void updateData<T>(T data)
        {
           
                var db = new SQLiteConnection(_databasePath);                
                    db.Update(data);
        }

        

        public string DeleteData<T>(T data)
        {
            try
            {
                var db = new SQLiteConnection(_databasePath);
                    db.Delete(data);
                return "Data deleted";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        public List<Client> RetrieveAllClients()
        {            
                var clients = new List<Client>();
                var db = new SQLiteConnection(_databasePath);
                var query = db.Table<Client>();
                foreach (var client in query)
                {
                    clients.Add(client);
                System.Diagnostics.Debug.Write(client.Id);
                }
                return clients;
        }

        public List<Shelf> RetrieveAllShelves()
        {
            var shelves = new List<Shelf>();
            var db = new SQLiteConnection(_databasePath);
            var query = db.Table<Shelf>();
            foreach (var shelf in query)
            {
                shelves.Add(shelf);
            }
            return shelves;
        }

        //Retrieve all shelves that have less than 20 books
        public List<Shelf> RetrieveAvailableShelves()
        {            
            var db = new SQLiteConnection(_databasePath);
            var shelves = (from book in db.Table<Book>()
                             group book by book.ShelfId into g
                             join shelf in db.Table<Shelf>() on g.Key equals shelf.Id
                             where g.Count() >= 20
                             select new Shelf()
                             {
                                 Id = shelf.Id,
                                 Number = shelf.Number,
                                 Description = shelf.Description
                             }).ToList();
            var allShelves = RetrieveAllShelves();
            var allShelveIds = allShelves.Select(x => x.Id).ToList();
            var availableShelveIds = allShelveIds.Except(shelves.Select(x => x.Id)).ToList();
            return allShelves.Where(x => availableShelveIds.Contains(x.Id)).ToList();
        }

        public List<int> RetrieveAvailableShelfBookNumbers(int shelfId, int currentBookNumberInShelf)
        {
            var db = new SQLiteConnection(_databasePath);
            var booksInShelf = (from book in db.Table<Book>()
                                where book.ShelfId == shelfId 
                                // For some reason selecting book.NumberInShelf gave 0 as value
                                // So I first get the books and then the NumberInShelf's
                                select book
                           ).ToList();
            var freeNumbers = Enumerable.Range(1, 20).ToList().Except(booksInShelf.Select(x => x.NumberInShelf)).ToList();
            // Add current book NumberInShelf also to free Numbers
            if(currentBookNumberInShelf != 0)
                freeNumbers.Add(currentBookNumberInShelf);
            return freeNumbers;
        }

        public bool IsBookAvailable(int bookId)
        {
            var books = new List<Book>();
            var db = new SQLiteConnection(_databasePath);
            var lastClientBook = db.Table<ClientBook>().Where(x=>x.BookId == bookId).OrderByDescending(x=>x.StartDate).FirstOrDefault();
            if(lastClientBook == null || lastClientBook.ReturnDate != null)
            {
                return true;
            }
            return false;                        
        }

        public bool HasMoreThanThreeBooks(int clientId)
        {
            var db = new SQLiteConnection(_databasePath);
            
            var foundClientId = (from clientBook in db.Table<ClientBook>()
                           where clientBook.ClientId == clientId 
                                 && clientBook.ReturnDate == null
                                 group clientBook by clientBook.ClientId into g
                           where g.Count() >= 3
                           select g.Key).FirstOrDefault();
            if (foundClientId != 0)
                return true;
            return false;
        }

        public bool HasBookOverDeadline(int clientId)
        {
            var db = new SQLiteConnection(_databasePath);

            var foundClient = (from clientBook in db.Table<ClientBook>()
                                 where clientBook.ClientId == clientId
                                       && clientBook.ReturnDate == null
                                       && clientBook.EndDate < DateTime.Now
                                 select clientBook
                                       ).FirstOrDefault();
            if (foundClient != null)
                return true;
            return false;
        }

        public void SetAllUntrustworthyClients()
        {
            var db = new SQLiteConnection(_databasePath);
            
            var clientIds = (from clientBook in db.Table<ClientBook>()
                                 where clientBook.EndDate < DateTime.Now && clientBook.ReturnDate == null
                                 group clientBook by clientBook.ClientId into g
                                 where g.Count() >= 1
                                 select g.Key).ToList();

            var clients = (from client in db.Table<Client>()
                           where clientIds.Contains(client.Id)
                           select client).ToList();

            clients.ForEach(x => x.Untrustworthy = true);

            db.UpdateAll(clients);            
        }


        public List<ClientBook> RetrieveAllClientBooks()
        {
            var clientBooks = new List<ClientBook>();
            var db = new SQLiteConnection(_databasePath);
            var clientBooksFromDb = db.Table<ClientBook>();
            foreach (var clientBook in clientBooksFromDb)
            {
                clientBooks.Add(clientBook);
                
            }
            return clientBooks;
        }


        public List<Book> RetrieveAllBooks()
        {
            var books = new List<Book>();
            var db = new SQLiteConnection(_databasePath);
            var query = db.Table<Book>();
            foreach (var book in query)
            {
                books.Add(book);
                System.Diagnostics.Debug.Write(book.Id);
            }
            return books;
        }
    
    }
}
