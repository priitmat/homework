﻿using HomeExercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HomeExercise
{
    /// <summary>
    /// Interaction logic for ClientBookWIndow.xaml
    /// </summary>
    public partial class ClientBookWindow : Window
    {
        DatabaseService _databaseService;
        List<ClientBook> _clientBooks;
        List<Book> _books;
        List<Client> _clients;
        public ClientBookWindow()
        {
            InitializeComponent();
            _databaseService = DatabaseService.Instance;
            _databaseService.SetAllUntrustworthyClients();
           _clientBooks = _databaseService.RetrieveAllClientBooks();
            if(_clientBooks.Count == 0)
            {
                buttonSave.IsEnabled = false;
            }
            _books = _databaseService.RetrieveAllBooks();
            _clients = _databaseService.RetrieveAllClients();
            

            listViewClientBooks.ItemsSource = _clientBooks;

            listViewClientBooks.SelectionChanged += ListViewClientBooks_SelectionChanged;
            if(_clientBooks.Count > 0)
                 listViewClientBooks.SelectedItem = _clientBooks.Last();

            comboBoxClient.ItemsSource = _clients;
            comboBoxBook.ItemsSource = _books;
            comboBoxBook.SelectionChanged += ComboBoxBook_SelectionChanged;           

            datePickerReturnDate.SelectedDateChanged += DatePickerReturnDate_SelectedDateChanged;
            datePickerEndDate.SelectedDateChanged += DatePickerEndDate_SelectedDateChanged;
            datePickerStartDate.SelectedDateChanged += DatePickerStartDate_SelectedDateChanged;

            _databaseService.SetAllUntrustworthyClients();
        }

        

        private void DatePickerStartDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (datePickerStartDate.SelectedDate != null && datePickerEndDate.SelectedDate < datePickerStartDate.SelectedDate)
            {
                MessageBox.Show("Start date cannot be after end date");
                datePickerStartDate.SelectedDate = null;
            }
        }

        private void DatePickerEndDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if(datePickerEndDate.SelectedDate != null && datePickerEndDate.SelectedDate < datePickerStartDate.SelectedDate)
            {
                MessageBox.Show("End date cannot be before start date");
                datePickerEndDate.SelectedDate = null;
            }
        }

        private void DatePickerReturnDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if(datePickerReturnDate.SelectedDate != null && datePickerReturnDate.SelectedDate > DateTime.Now)
            {
                MessageBox.Show("Return date cannot be in future");
                datePickerReturnDate.SelectedDate = null;
            }
        }

        private void ComboBoxBook_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (comboBoxBook.SelectedItem != null && listViewClientBooks.SelectedItem != null &&
                ((ClientBook)listViewClientBooks.SelectedItems[0]).BookId != ((Book)comboBoxBook.SelectedItem).Id && 
                !_databaseService.IsBookAvailable(((Book)comboBoxBook.SelectedItem).Id))
            {
                MessageBox.Show("Book unavailable");
                comboBoxBook.SelectedItem = null;
            }
        }

        private void ListViewClientBooks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listViewClientBooks.SelectedItem != null)
            {
                var selectedClientBook = (ClientBook)listViewClientBooks.SelectedItems[0];

                datePickerStartDate.SelectedDate = selectedClientBook.StartDate;   
                datePickerReturnDate.SelectedDate = selectedClientBook.ReturnDate;  
                datePickerEndDate.SelectedDate = selectedClientBook.EndDate;
                comboBoxClient.SelectedValue = _clients.Where(x => x.Id == selectedClientBook.ClientId).FirstOrDefault();
                comboBoxBook.SelectedValue = _books.Where(x => x.Id == selectedClientBook.BookId).FirstOrDefault();
            }
        }

        private void buttonAddClientBookWindow_Click(object sender, RoutedEventArgs e)
        {
            AddClientBookWindow addClientBookWindow = new AddClientBookWindow();
            addClientBookWindow.Show();
            this.Close();
        }

        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = (ClientBook)listViewClientBooks.SelectedItem;
            selectedItem.StartDate = datePickerStartDate.SelectedDate;
            selectedItem.EndDate = datePickerEndDate.SelectedDate;
            selectedItem.ReturnDate = datePickerReturnDate.SelectedDate;
            selectedItem.BookId = ((Book)comboBoxBook.SelectedValue).Id;
            selectedItem.ClientId = ((Client)comboBoxClient.SelectedValue).Id;

            _databaseService.updateData<ClientBook>(selectedItem);
            listViewClientBooks.ItemsSource = _databaseService.RetrieveAllClientBooks();          
        }

        private void buttonDelete_Click(object sender, RoutedEventArgs e)
        {
            ClientBook clientBookToDelete = (ClientBook)listViewClientBooks.SelectedItems[0];
            var result = _databaseService.DeleteData<ClientBook>(clientBookToDelete);
            listViewClientBooks.ItemsSource = _databaseService.RetrieveAllClientBooks();
        }
    }
}
