﻿using HomeExercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HomeExercise
{
    /// <summary>
    /// Interaction logic for ClientWindow.xaml
    /// </summary>
    public partial class ClientWindow : Window
    {

        DatabaseService _databaseService;
        public ClientWindow()
        {
            InitializeComponent();
            _databaseService = DatabaseService.Instance;            
            var allClients = _databaseService.RetrieveAllClients();
            listView.ItemsSource = allClients;
            
            listView.SelectionChanged += ListView_SelectionChanged;         
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listView.SelectedItem != null)
            {
                Client selectedClient = (Client)listView.SelectedItem;
                textBoxName.Text = selectedClient.Name;
                datePickerDateOfBirth.SelectedDate = selectedClient.DateOfBirth;
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {          
            var newClient = new Client();
            newClient.Name= textBoxName.Text;
            newClient.DateOfBirth = datePickerDateOfBirth.SelectedDate;
            _databaseService.insertData(newClient);
            listView.ItemsSource = _databaseService.RetrieveAllClients();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            Client clientToDelete = (Client)listView.SelectedItem;
            var result= _databaseService.DeleteData<Client>(clientToDelete);
            listView.ItemsSource = _databaseService.RetrieveAllClients();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var selectedClient = (Client)listView.SelectedItem;
            selectedClient.Name = textBoxName.Text;
            selectedClient.DateOfBirth = datePickerDateOfBirth.SelectedDate;            
            _databaseService.updateData<Client>(selectedClient);
            listView.ItemsSource = _databaseService.RetrieveAllClients();
        }
    }
}
