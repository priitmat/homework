﻿using HomeExercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HomeExercise
{
    /// <summary>
    /// Interaction logic for AddClientBookWindow.xaml
    /// </summary>
    public partial class AddClientBookWindow : Window
    {
        DatabaseService _databaseService;
        List<Client> _clients;
        List<Book> _books;

        public AddClientBookWindow()
        {
            InitializeComponent();
            _databaseService = DatabaseService.Instance;
            _clients = _databaseService.RetrieveAllClients();
            _books = _databaseService.RetrieveAllBooks();
            comboBoxClient.ItemsSource = _clients;
            comboBoxBook.ItemsSource = _books;
            comboBoxBook.SelectionChanged += ComboBoxBook_SelectionChanged;
            comboBoxClient.SelectionChanged += ComboBoxClient_SelectionChanged;
                

            datePickerStartDate.SelectedDateChanged += DatePickerStarDate_SelectedDateChanged;
            datePickerReturnDate.SelectedDateChanged += DatePickerReturnDate_SelectedDateChanged;
            datePickerEndDate.SelectedDateChanged += DatePickerEndDate_SelectedDateChanged;
        }

        private void ComboBoxClient_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(comboBoxClient.SelectedItem != null)
            {
                var client = ((Client)comboBoxClient.SelectedItem);
                if (client.Id != 0 && comboBoxClient.SelectedItem != null)
                {
                    if(client.Untrustworthy.HasValue && client.Untrustworthy.Value)
                    {
                        MessageBox.Show("Client is untrustworthy");
                        comboBoxClient.SelectedItem = null;
                    }   

                    else if (_databaseService.HasBookOverDeadline(client.Id))
                    {
                        MessageBox.Show("Client has book over deadline");
                        comboBoxClient.SelectedItem = null;
                    }
                    else if (_databaseService.HasMoreThanThreeBooks(client.Id))
                    {
                        MessageBox.Show("Client has already 3 books");
                        comboBoxClient.SelectedItem = null;
                    }
                }
            }
            
        }

        private void DatePickerEndDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (datePickerEndDate.SelectedDate != null && datePickerEndDate.SelectedDate < datePickerStartDate.SelectedDate)
            {
                MessageBox.Show("End date cannot be before start date");
                datePickerEndDate.SelectedDate = null;
            }
        }

        private void DatePickerReturnDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (datePickerReturnDate.SelectedDate != null && datePickerReturnDate.SelectedDate > DateTime.Now)
            {
                MessageBox.Show("Return date cannot be in future");
                datePickerReturnDate.SelectedDate = null;
            }
        }

        private void DatePickerStarDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (datePickerStartDate.SelectedDate != null && datePickerEndDate.SelectedDate < datePickerStartDate.SelectedDate)
            {
                MessageBox.Show("Start date cannot be after end date");
                datePickerStartDate.SelectedDate = null;
            }
        }

        private void ComboBoxBook_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxBook.SelectedItem != null)
            {
                var selectedBook = ((Book)comboBoxBook.SelectedItem);
                var isBookAvailable = _databaseService.IsBookAvailable(selectedBook.Id);
                if (!isBookAvailable)
                {
                    MessageBox.Show("Book is lent out");
                    comboBoxBook.SelectedItem = null;
                }


            }
        }

        private void buttonAddClientBook_Click(object sender, RoutedEventArgs e)
        {
            var newClientBook = new ClientBook();
            newClientBook.StartDate = datePickerStartDate.SelectedDate;
            newClientBook.EndDate = datePickerEndDate.SelectedDate;
            newClientBook.ReturnDate = datePickerReturnDate.SelectedDate;
            newClientBook.ClientId = ((Client)comboBoxClient.SelectedItem).Id;
            newClientBook.BookId = ((Book)comboBoxBook.SelectedItem).Id;
             _databaseService.insertData<ClientBook>(newClientBook);
            ClientBookWindow clientBookWindow = new ClientBookWindow();
            clientBookWindow.Show();
            this.Close();
        }
    }
}
