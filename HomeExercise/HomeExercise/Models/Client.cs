﻿
using SQLite;
using System;

namespace HomeExercise.Models
{
    public class Client
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public bool? Untrustworthy { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
  
}
