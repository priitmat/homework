﻿using SQLite;

namespace HomeExercise.Models
{
    public class Shelf
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1} ", Number, Description);
        }
    }
}
