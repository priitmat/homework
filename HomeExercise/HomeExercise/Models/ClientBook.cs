﻿using SQLite;
using System;

namespace HomeExercise.Models
{
    public class ClientBook
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int BookId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? ReturnDate { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} ", ClientId, BookId);
        }
    }


}
