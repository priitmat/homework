﻿using SQLite;

namespace HomeExercise.Models
{
    public class Book
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string IdentifierCode { get; set; }
        public int Age { get; set; }
        public int DaysForLoan { get; set; }
        public int ShelfId { get; set; }
        public int NumberInShelf { get; set; }

        public override string ToString()
        {
            return string.Format("{0:20} {1:20} {2:20}", Title, Name, IdentifierCode);
        }
    }    
}
