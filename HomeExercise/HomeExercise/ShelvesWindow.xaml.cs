﻿using HomeExercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HomeExercise
{
    /// <summary>
    /// Interaction logic for ShelvesWindow.xaml
    /// </summary>
    public partial class ShelvesWindow : Window
    {
        DatabaseService _databaseService;

        public ShelvesWindow()
        {
            InitializeComponent();
            _databaseService = DatabaseService.Instance;
            var shelves = _databaseService.RetrieveAllShelves();
            listView.ItemsSource = shelves;
            listView.SelectionChanged += ListView_SelectionChanged;
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listView.SelectedItem != null)
            {
                Shelf selectedShelf = (Shelf)listView.SelectedItem;
                textBoxDescription.Text = selectedShelf.Description;
                textBoxNumber.Text = selectedShelf.Number.ToString();
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var newShelf = new Shelf();
            newShelf.Description = textBoxDescription.Text;
            newShelf.Number = Convert.ToInt32( textBoxNumber.Text);
            _databaseService.insertData(newShelf);
            listView.ItemsSource = _databaseService.RetrieveAllShelves();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            Shelf shelfToDelete = (Shelf)listView.SelectedItem;
            var result = _databaseService.DeleteData<Shelf>(shelfToDelete);
            listView.ItemsSource = _databaseService.RetrieveAllShelves();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var selectedShelf = (Shelf)listView.SelectedItem;
            selectedShelf.Description = textBoxDescription.Text;
            selectedShelf.Number = Convert.ToInt32(textBoxNumber.Text);
            _databaseService.updateData<Shelf>(selectedShelf);
            listView.ItemsSource = _databaseService.RetrieveAllShelves();
        }
    }
}
