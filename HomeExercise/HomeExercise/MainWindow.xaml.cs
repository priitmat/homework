﻿using HomeExercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HomeExercise
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //var dbService = DatabaseService.Instance;
            //var shelf = new Shelf();
            //shelf.Number =2 ;
            //dbService.insertData(shelf);
            //dbService.RetrieveAvailableShelves();
            // dbService.RetrieveAvailableShelfBookNumbers(1);
        }

        private void buttonClientWindow_Click(object sender, RoutedEventArgs e)
        {
            ClientWindow clientWindow = new ClientWindow();
            clientWindow.Show();
        }

        private void buttonBookWindow_Click(object sender, RoutedEventArgs e)
        {
            BookWindow bookWindow = new BookWindow();
            bookWindow.Show();
        }

        private void buttonClientBookWindow_Click(object sender, RoutedEventArgs e)
        {
            ClientBookWindow clientBookWindow = new ClientBookWindow();
            clientBookWindow.Show();
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            ShelvesWindow shelvesWindow = new ShelvesWindow();
            shelvesWindow.Show();
        }
    }
}
