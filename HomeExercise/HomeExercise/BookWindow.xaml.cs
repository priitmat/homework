﻿using HomeExercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HomeExercise
{
    /// <summary>
    /// Interaction logic for BookWindow.xaml
    /// </summary>
    public partial class BookWindow : Window
    {
        DatabaseService _databaseService;
        List<Shelf> _availableShelves;
        public BookWindow()
        {
            InitializeComponent();
             _databaseService = DatabaseService.Instance;            
            _availableShelves = _databaseService.RetrieveAvailableShelves();
            comboBoxShelf.ItemsSource = _availableShelves;
            comboBoxShelf.DisplayMemberPath = "Number";
            comboBoxShelf.SelectionChanged += ComboBox_SelectionChanged;
            var allBooks = _databaseService.RetrieveAllBooks();     
            listViewBooks.ItemsSource = allBooks;
            listViewBooks.SelectionChanged += ListView_SelectionChanged;
            if(allBooks.Count > 0)
                listViewBooks.SelectedItem = allBooks.Last();

            comboBoxNumberInShelf.SelectionChanged += ComboBoxNumberInShelf_SelectionChanged;
        }

        private void ComboBoxNumberInShelf_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedBook = (Book)listViewBooks.SelectedItems[0];

            textBoxIdentifierCode.Text = ((Shelf)comboBoxShelf.SelectedItem).Number + " - " + comboBoxNumberInShelf.SelectedValue;
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listViewBooks.SelectedItem != null)
            {
                var selectedBook = (Book)listViewBooks.SelectedItems[0];
                textBoxTitle.Text = selectedBook.Title;
                textBox1Author.Text = selectedBook.Name;
                comboBoxShelf.SelectedItem = _availableShelves.Where(x => x.Id == selectedBook.ShelfId).FirstOrDefault();
                textBoxAge.Text = selectedBook.Age.ToString();
                textBoxDaysForLoan.Text = selectedBook.DaysForLoan.ToString();
                textBoxIdentifierCode.Text = selectedBook.ShelfId.ToString() + " - " + selectedBook.NumberInShelf.ToString();
            }
        }


        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(listViewBooks.SelectedItems.Count > 0)
            {
                var selectedBook = (Book)listViewBooks.SelectedItems[0];
                comboBoxNumberInShelf.ItemsSource = _databaseService.RetrieveAvailableShelfBookNumbers(selectedBook.ShelfId, selectedBook.NumberInShelf);
                comboBoxNumberInShelf.SelectedValue = selectedBook.NumberInShelf;
                textBoxIdentifierCode.Text = ((Shelf)comboBoxShelf.SelectedItem).Number + " - " + comboBoxNumberInShelf.SelectedValue;
            }           
        }

        private void button_Click(object sender, RoutedEventArgs e)
        { 
            AddBookWindow addBookWindow = new AddBookWindow();
            addBookWindow.Show();
            this.Close();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = (Book)listViewBooks.SelectedItem;
            selectedItem.Title = textBoxTitle.Text;
            selectedItem.Name = textBox1Author.Text;
            selectedItem.Age = Int32.Parse(textBoxAge.Text);
            selectedItem.DaysForLoan = Int32.Parse(textBoxDaysForLoan.Text);
            var selectedShelf = (Shelf)comboBoxShelf.SelectedItem;
            selectedItem.ShelfId = selectedShelf.Id;
            selectedItem.NumberInShelf = (int)comboBoxNumberInShelf.SelectedValue;
            _databaseService.updateData<Book>(selectedItem);
            listViewBooks.ItemsSource = _databaseService.RetrieveAllBooks();           
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            Book bookToDelete = (Book)listViewBooks.SelectedItems[0];
            var result = _databaseService.DeleteData<Book>(bookToDelete);
            listViewBooks.ItemsSource = _databaseService.RetrieveAllBooks();
        }
    }
}
